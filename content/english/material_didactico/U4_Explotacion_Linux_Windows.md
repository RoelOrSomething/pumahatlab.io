---
title: "Explotacion en Linux y Windows"
date: 2022-10-23T00:00:00+00:00
author: PHCT
image_webp: images/blog/material/u4_explotacion_linux_windows.webp
image: images/blog/material/u4_explotacion_linux_windows.webp
description : "Unidad 4. Explotación en Linux y Windows."
categories: ["Material didactico"]
tags: ["escalamiento", "linux", "windows", "sistemas operativos"]
---

## Explotación en Linux y Windows

En la actualidad, la mayoría de los servidores se ejecutan sobre un sistema de Windows o un sistema Linux. Una vez que se tiene un pie adentro de la organización, es necesario conocer bien el entorno operativo para poder hacer un escalamiento de privilegios y tener acceso a los recursos más críticos del sistema.

### Material didáctico

[Explotacion en Linux y Windows](https://gitlab.com/PumaHat/pumahat.gitlab.io/-/tree/main/content/english/material/U4_Explotacion_Linux_Windows)